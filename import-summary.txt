ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From FtrScanDemoUsbHost:
* proguard\
* proguard\dump.txt
* proguard\mapping.txt
* proguard\seeds.txt
* proguard\usage.txt
From ftrScanApiAndroidHelperUsbHost:
* proguard.cfg
From ftrWsqAndroidHelper:
* proguard-project.txt

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In ftrScanApiAndroidHelperUsbHost:
* AndroidManifest.xml => ftrScanApiAndroidHelperUsbHost\src\main\AndroidManifest.xml
* assets\ => ftrScanApiAndroidHelperUsbHost\src\main\assets
* jni\ => ftrScanApiAndroidHelperUsbHost\src\main\jni\
* libs\arm64-v8a\libftrScanAPI.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\arm64-v8a\libftrScanAPI.so
* libs\arm64-v8a\libftrScanApiAndroidJni.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\arm64-v8a\libftrScanApiAndroidJni.so
* libs\arm64-v8a\libusb-1.0.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\arm64-v8a\libusb-1.0.so
* libs\armeabi-v7a\libftrScanAPI.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\armeabi-v7a\libftrScanAPI.so
* libs\armeabi-v7a\libftrScanApiAndroidJni.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\armeabi-v7a\libftrScanApiAndroidJni.so
* libs\armeabi-v7a\libusb-1.0.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\armeabi-v7a\libusb-1.0.so
* libs\armeabi\libftrScanAPI.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\armeabi\libftrScanAPI.so
* libs\armeabi\libftrScanApiAndroidJni.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\armeabi\libftrScanApiAndroidJni.so
* libs\armeabi\libusb-1.0.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\armeabi\libusb-1.0.so
* libs\x86\libftrScanAPI.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\x86\libftrScanAPI.so
* libs\x86\libftrScanApiAndroidJni.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\x86\libftrScanApiAndroidJni.so
* libs\x86\libusb-1.0.so => ftrScanApiAndroidHelperUsbHost\src\main\jniLibs\x86\libusb-1.0.so
* lint.xml => ftrScanApiAndroidHelperUsbHost\lint.xml
* res\ => ftrScanApiAndroidHelperUsbHost\src\main\res\
* src\ => ftrScanApiAndroidHelperUsbHost\src\main\java\
In ftrWsqAndroidHelper:
* AndroidManifest.xml => ftrWsqAndroidHelper\src\main\AndroidManifest.xml
* assets\ => ftrWsqAndroidHelper\src\main\assets
* jni\ => ftrWsqAndroidHelper\src\main\jni\
* libs\arm64-v8a\libftrWSQAndroid.so => ftrWsqAndroidHelper\src\main\jniLibs\arm64-v8a\libftrWSQAndroid.so
* libs\arm64-v8a\libftrWSQAndroidJni.so => ftrWsqAndroidHelper\src\main\jniLibs\arm64-v8a\libftrWSQAndroidJni.so
* libs\armeabi-v7a\libftrWSQAndroid.so => ftrWsqAndroidHelper\src\main\jniLibs\armeabi-v7a\libftrWSQAndroid.so
* libs\armeabi-v7a\libftrWSQAndroidJni.so => ftrWsqAndroidHelper\src\main\jniLibs\armeabi-v7a\libftrWSQAndroidJni.so
* libs\armeabi\libftrWSQAndroid.so => ftrWsqAndroidHelper\src\main\jniLibs\armeabi\libftrWSQAndroid.so
* libs\armeabi\libftrWSQAndroidJni.so => ftrWsqAndroidHelper\src\main\jniLibs\armeabi\libftrWSQAndroidJni.so
* libs\x86\libftrWSQAndroid.so => ftrWsqAndroidHelper\src\main\jniLibs\x86\libftrWSQAndroid.so
* libs\x86\libftrWSQAndroidJni.so => ftrWsqAndroidHelper\src\main\jniLibs\x86\libftrWSQAndroidJni.so
* res\ => ftrWsqAndroidHelper\src\main\res\
* src\ => ftrWsqAndroidHelper\src\main\java\
In FtrScanDemoUsbHost:
* AndroidManifest.xml => ftrScanDemoUsbHost\src\main\AndroidManifest.xml
* assets\ => ftrScanDemoUsbHost\src\main\assets
* libs\activation.jar => ftrScanDemoUsbHost\libs\activation.jar
* libs\additionnal.jar => ftrScanDemoUsbHost\libs\additionnal.jar
* libs\android-support-v4.jar => ftrScanDemoUsbHost\libs\android-support-v4.jar
* libs\AndroidNBioBSPJNI.jar => ftrScanDemoUsbHost\libs\AndroidNBioBSPJNI.jar
* libs\google-play-services.jar => ftrScanDemoUsbHost\libs\google-play-services.jar
* libs\httpmime-4.2.1.jar => ftrScanDemoUsbHost\libs\httpmime-4.2.1.jar
* libs\mail.jar => ftrScanDemoUsbHost\libs\mail.jar
* libs\volley.jar => ftrScanDemoUsbHost\libs\volley.jar
* lint.xml => ftrScanDemoUsbHost\lint.xml
* proguard.cfg => ftrScanDemoUsbHost\proguard.cfg
* res\ => ftrScanDemoUsbHost\src\main\res\
* src\ => ftrScanDemoUsbHost\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
