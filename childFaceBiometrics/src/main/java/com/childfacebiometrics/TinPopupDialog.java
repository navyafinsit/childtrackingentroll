package com.childfacebiometrics;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

public class TinPopupDialog extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tin_popup_dialog);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.x = -20;
        //params.height = 300;
       // params.width = 550;
       // params.y = -10;
        params.gravity=10;


        this.getWindow().setAttributes(params);


        TinDetailsBean fullObject=(TinDetailsBean)getIntent().getSerializableExtra("info");

        TextView tv=(TextView)findViewById(R.id.cname);
        tv.setText(fullObject.getName());
        //Toast.makeText(TinPopupDialog.this, "You have chosen: " + " " + fullObject.getName(), Toast.LENGTH_LONG).show();

    }
}
