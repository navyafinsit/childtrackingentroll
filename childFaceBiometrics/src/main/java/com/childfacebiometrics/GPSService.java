package com.childfacebiometrics;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.telephony.SmsManager;


public class GPSService extends Service{
	
		private LocationManager locationManager=null;
		private LocationListener locationListener=null;
		Location curLocation;
		boolean locationChanged = false;  
		String deviceid;
		String ipid; 
		long distance;
		long time;
		
		//code added by pramod
		boolean requestLocationUpdate=false;
		boolean requestEmailLog = false;
		boolean stopService=false;
		boolean deleteBackUPData = false;
		private String controlRoomMobileNumber;
		
		float batteryPct;
		boolean serviceCreated = false;
		Intent intent = null;
		String fileName = "DataBackUp.txt";  
		
		File root = Environment.getExternalStorageDirectory();
		DateFormat utilDateFormatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
		
		DatabaseHandler dbHandler = new DatabaseHandler(this);
		SharedPreferences data;
		
		Timer timer = new Timer();
		
		Executor executor = Executors.newCachedThreadPool();		
		
		Handler handler = new Handler();
		
		
		long manualPollUpdatesTime = 300000;
		
		SmsManager sms = SmsManager.getDefault();	
		
		
		@Override
		public IBinder onBind(Intent arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		
		@Override
		protected void finalize() throws Throwable {
			// TODO Auto-generated method stub
			super.finalize();
		}
	
		@Override
		public void onConfigurationChanged(Configuration newConfig) {
			// TODO Auto-generated method stub
			super.onConfigurationChanged(newConfig);
			
		}
	
		@Override
		public void onLowMemory() {
			// TODO Auto-generated method stub
			super.onLowMemory();
			isNotEntered = true;
			//System.out.println("LOW MEMORY CALLED ********************************************");
			Intent intent = new Intent("com.quad.tracking.receive");
			sendBroadcast(intent);
		}
	
		@Override
		public void onRebind(Intent intent) {
			// TODO Auto-generated method stub
			super.onRebind(intent);
		}
	
		@Override
		public boolean onUnbind(Intent intent) {
			// TODO Auto-generated method stub
			return super.onUnbind(intent);
		}
	
		@Override
		public void onDestroy() {
			
			if(data == null) {
				data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
			}
			
			stopService     = data.getBoolean("stopservice", false);
			isNotEntered = true;
			
			if(timer != null)
				timer.cancel(); 
				
			if(locationManager != null && locationListener != null)
				locationManager.removeUpdates(locationListener);
			
			if(handler != null)
				handler.removeCallbacks(runnable);
			
			if(!stopService) {				
				Intent intent = new Intent("com.quad.tracking.receive");
				sendBroadcast(intent); 
			}
			
			if(stopService) {
				SharedPreferences.Editor editor = data.edit();
				editor.putBoolean("stopservice", false);
         		editor.commit();
         	}
			
		}
		
		
		
		@Override
		public void onCreate() {
			
			isNotEntered = true;
			data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
		    stopService  = data.getBoolean("stopservice", false);
			
			if(stopService) {
				SharedPreferences.Editor editor = data.edit();
				editor.putBoolean("stopservice", false);
         		editor.commit();
         		data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
			}
			
		    
		    
			deviceid = data.getString("deviceid", null);
			ipid = data.getString("ipid", IPInfo.IPADDRESS);
			distance = data.getLong("distance",30);
			time = data.getLong("time",0);
			manualPollUpdatesTime = data.getLong("manualtimer", 300000);
			controlRoomMobileNumber =  data.getString("controlRoomMobileNumber", controlRoomMobileNumber);
			requestLocationUpdate = data.getBoolean("requestLocationUpdate", false);
			requestEmailLog  = data.getBoolean("requestemaillog", false);
			deleteBackUPData = data.getBoolean("deletebackupdata", false);
	        
		
			if(requestEmailLog) {
				
				ConnectionDetector connectionDetector=new ConnectionDetector(GPSService.this); 
    			
				try {
					if(connectionDetector.isConnectingToInternet()) {
						MailTask mailTask = new MailTask();
						mailTask.execute("MAIL");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
			}
			
			if(deleteBackUPData) {
				
				try {
					DatabaseHandler dbHandler = new DatabaseHandler(GPSService.this);
					dbHandler.delete();
					SharedPreferences.Editor editor = data.edit();
					editor.putBoolean("deletebackupdata", false);
					editor.commit();					
				} catch (Exception e) {
					e.printStackTrace();					
				}
				
			}
			
			////System.out.println("ONCREATE CALLED ..................................................");
			getLocations();
			
			handler.postDelayed(runnable, 1800000);
			            
			if(controlRoomMobileNumber != null && controlRoomMobileNumber.trim().length() > 0 && requestLocationUpdate) {
				//SEND THROUGH INTERNET IF AVAILABLE ELSE SEND SMS TO CONTROL NUMBER. DO IT USING ASYNC TASK
				
				SharedPreferences.Editor editor = data.edit();
				
				RequestLocationTask requestLocationTask = new RequestLocationTask();	
				requestLocationTask.execute(controlRoomMobileNumber);
				
				editor.putString("controlRoomMobileNumber", null);
       		 	editor.putBoolean("requestLocationUpdate", false);
       		 	editor.commit();
				
				
			}
			
			
			pollForUpdates();
		}
		
		class RequestLocationTask extends AsyncTask<String, String, String> {  


		    protected String doInBackground(String... urls) {
		    	
		    	String controlRoomMobileNumber =  urls[0];
		    	
		    	ConnectionDetector connectionDetector = new ConnectionDetector(getApplicationContext());
		    	
		    	boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		    	
		    	Location lastKnownLocation = null;
		    	String currentDateTimeString="";
		    	
		    	currentDateTimeString = utilDateFormatter.format(new Date());
		    	
		    	if(connectionDetector.isConnectingToInternet()) {
		    		//INTERNET ON
		    		
		    		if(isGPSEnabled) {
		    			
		    			//INTERNET AND GPS BOTH ARE  ON
		    			if(locationManager != null) {
			    			lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			    		}
			    		
		    			if(lastKnownLocation != null) {
			    			
			    			SharedPreferences data = getSharedPreferences("locationdata", Context.MODE_WORLD_READABLE);
			    		   		    		  
			    		    curLocation = lastKnownLocation;
			    	        
			    	        double lat = curLocation.getLatitude();
			    	        double lon = curLocation.getLongitude();  
			    	        
			    			SharedPreferences.Editor editor = data.edit();
			    			editor.putString("latitude", lat+"");
			    			editor.putString("longitude", lon+"");
			    			editor.commit();
			    			
			    		    
			    	        
			    	        try {
			    	        		Intent battery= getApplicationContext().registerReceiver(null,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
			    			        int level = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
			    			        int scale = battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
			    			        batteryPct = level / (float)scale *100;
			    	        	  
			    			        distance = 0L;
			    		        	
			    		    		String urlStr = "http://"+ipid+"/childface/andromobium?latitude="+curLocation.getLatitude()+"&longitude="+curLocation.getLongitude()+"&deviceid="+deviceid+"&datetime="+currentDateTimeString+"&battery="+batteryPct+"&distance="+distance+"";
			    		    		urlStr = urlStr.toString().replace(" ", "%20");
			    		      		URL url = new URL(urlStr);
			    		      		
			    		    		HttpURLConnection deviceData = (HttpURLConnection) url.openConnection();
			    		    		deviceData.setRequestMethod("POST");
			    		    		deviceData.setConnectTimeout(45000);
			    		      		
			    		    		BufferedReader in = new BufferedReader(new InputStreamReader( deviceData.getInputStream()));
			    		    		String inputLine;
			    		      		
			    	    			while ((inputLine = in.readLine()) != null) {
			    	    			   
			    	    			}
			    	    			
			    	    			in.close();
			    		    	
			    	        } catch (Exception e) {
			    	        	
			    	        	//IF INTERNET IS AVAILABLE AND SERVER FAILED TO PROCESS REQUEST THEN SEND SMS TO CONTROL NUMBER
			    	        	
			    	        	
			    	        	DecimalFormat df = new DecimalFormat("#.######");
				    			String latitude = df.format(lat);
							    String longitude = df.format(lon);
				    							    	        
				    	        try {
				    	        	  
				    		        	Intent battery= getApplicationContext().registerReceiver(null,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
				    			        int level = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
				    			        int scale = battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
				    			        batteryPct = level / (float)scale *100;
				    	        	  
				    			        distance = 0L;
				    		        	
				    			        String message ="CL@!"+latitude+"!"+longitude+"!"+deviceid+"!"+currentDateTimeString+"!"+batteryPct+"!"+distance+"!"+IPInfo.IPADDRESS;
				    			        List<String> strList = getParts(message, 150);
				    			       
						    			for (String messagePart : strList) {
						    				if(sms != null) {
						    					try {
													sms.sendTextMessage(controlRoomMobileNumber,null, messagePart, null, null);
												} catch (Exception e1) {
													e1.printStackTrace();
												}
						    				}
					         		   }
				    		    		
				    		    	
				    	        } catch (Exception e2) {
				    	        	
				    	        	
				    			}
			    	        	
			    			}
			    	        
			    	        
			    	        return "";
			    	        
			    	       
			    	    
			    		} else {
			    			//SEND SMS TO CONTROL NUMBER INDICATING DEVICE IS SEARCHING FOR LOCATION AND COULD NOT DETERMINE WHEN INTERNET IS ON.
			    			
			    			
			    			String message = "DS@!SEARCHING FOR LOCATION. TRY AGAIN LATER!"+deviceid+"!"+currentDateTimeString+"!"+IPInfo.IPADDRESS+"";
			    			List<String> strList = getParts(message, 150);
			    			
			    			for (String messagePart : strList) {
			    				if(sms != null) {
			    					try {
			    						
										sms.sendTextMessage(controlRoomMobileNumber,null, messagePart, null, null);
										
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
			    				}
		         		   }
			    			
			    		   return "";
			    			
			    		}
		    		} else {
		    			//SEND SMS TO CONTROL NUMBER INDICATING GPS IS DISABLED
		    			
		    			
		    			String message = "DS@!GPS IS DISABLED ON DEVICE!"+deviceid+"!"+currentDateTimeString+"!"+IPInfo.IPADDRESS+"";
		    			List<String> strList = getParts(message, 150);
		    			
		    			for (String messagePart : strList) {
		    				if(sms != null) {
		    					try {
									sms.sendTextMessage(controlRoomMobileNumber,null, messagePart, null, null);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
		    				}
	         		   }
		    			
		    		   return "";	
		    			
		    		}
		    		
		    	} else {	//IS CONNECTING TO INTERNET IS TRUE (UPPER PART) AND FALSE (LOWER PART)
		    		
		    		//SEND SMS TO CONTROL NUMBER
		    		
		    		//CASE I : IF INTERNET IS NOT AVAILABLE AND GPS IS DISABLED 
		    		
		    		//SEND SMS TO CONTROL NUMBER INDICATING INTERNET AND GPS IS DISABLED
		    		
		    		
	    			
			    		if(!isGPSEnabled) {
			    			
			    			String message = "DS@!GPS AND INTERNET BOTH ARE DISABLED ON DEVICE!"+deviceid+"!"+currentDateTimeString+"!"+IPInfo.IPADDRESS+"";
			    			List<String> strList = getParts(message, 150);
			    			
			    			for (String messagePart : strList) {
			    				if(sms != null) {
			    					try {
										sms.sendTextMessage(controlRoomMobileNumber,null, messagePart, null, null);
									} catch (Exception e) {
										e.printStackTrace();
									}
			    				}
		         		   }
			    			
			    		   return "";
			    		
			    	   } 
			    		
			    	   //CASE II : IF INTERNET IS NOT AVAILABLE AND GPS IS ENABLED
			    		
			    	   //SEND SMS TO CONTROL NUMBER GPS COORDINATES
			    		
			    	   if(isGPSEnabled) {
			    		   
			    		   if(locationManager != null) {
				    			lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				    		}
				    		
			    			if(lastKnownLocation != null) {
				    			
				    			SharedPreferences data = getSharedPreferences("locationdata", Context.MODE_WORLD_READABLE);
				    		   		    		  
				    		    curLocation = lastKnownLocation;
				    	        
				    	        double lat = curLocation.getLatitude();
				    	        double lon = curLocation.getLongitude();    
				    			SharedPreferences.Editor editor = data.edit();
				    			editor.putString("latitude", lat+"");
				    			editor.putString("longitude", lon+"");
				    			editor.commit();
				    			DecimalFormat df = new DecimalFormat("#.######");
				    			String latitude = df.format(lat);
							    String longitude = df.format(lon);
				    			currentDateTimeString = utilDateFormatter.format(new Date());
				    	        
				    	        try {
				    	        	  
				    		        	Intent battery= getApplicationContext().registerReceiver(null,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
				    			        int level = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
				    			        int scale = battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
				    			        batteryPct = level / (float)scale *100;
				    	        	  
				    			        distance = 0L;
				    		        	
				    			        String message ="CL@!"+latitude+"!"+longitude+"!"+deviceid+"!"+currentDateTimeString+"!"+batteryPct+"!"+distance+"!"+IPInfo.IPADDRESS;
				    			        List<String> strList = getParts(message, 150);
				    			       
						    			for (String messagePart : strList) {
						    				if(sms != null) {
						    					try {
													sms.sendTextMessage(controlRoomMobileNumber,null, messagePart, null, null);
												} catch (Exception e) {
													e.printStackTrace();
												}
						    				}
					         		   }
				    		    		
				    		    	
				    	        } catch (Exception e) {
				    	        	
				    	        	
				    			}
			    		 
			    	          } else {
			    	        	  
			    	        	//SEND SMS TO CONTROL NUMBER INDICATING DEVICE IS SEARCHING FOR LOCATION AND COULD NOT DETERMINE WHEN INTERNET IS OFF.
					    			
					    			
					    			String message = "DS@!SEARCHING FOR LOCATION. TRY AGAIN LATER!"+deviceid+"!"+currentDateTimeString+"!"+IPInfo.IPADDRESS+"";
					    			List<String> strList = getParts(message, 150);
					    			
					    			for (String messagePart : strList) {
					    				if(sms != null) {
					    					try {
					    						
												sms.sendTextMessage(controlRoomMobileNumber,null, messagePart, null, null);
												
											} catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
					    				}
				         		   }
			    	        	  
			    	        	  return "";
			    	        	  
			    	          }
		    		
		    	 }
		    	
		        
		    }
		    return "";	
		    
		    }

		    protected void onPostExecute(String data) {
		    	
		    }
		}
		
		
		private static List<String> getParts(String string, int partitionSize) {
	        List<String> parts = new ArrayList<String>();
	        int len = string.length();
	        for (int i=0; i<len; i+=partitionSize)
	        {
	            parts.add(string.substring(i, Math.min(len, i + partitionSize)));
	        }
	        return parts;
	    }
		
		
		
		
		
		@Override
		public int onStartCommand(Intent intent, int flags, int startId) {  
				
			this.intent = intent;
			isNotEntered = true;
			
			//System.out.println("ONStart CALLED ..................................................");
			
			if(!serviceCreated) {
				
				if(intent != null) {
					
					Bundle b = intent.getExtras();
					
					//System.out.println("ONStart CALLED Intent .................................................. "+b);
					
					if(b != null) {
						
						deviceid = b.getString("deviceid");
						ipid = b.getString("ipid");
						distance = b.getLong("distance"); 
						time = b.getLong("time");
						manualPollUpdatesTime = b.getLong("manualtimer");
						
						if(locationManager == null) {
							getLocations();
						}
						
					} 
				
				}
				
				serviceCreated = true;
				
			}
			
			
			if(locationManager == null) {
				getLocations();
			}
			
			return START_STICKY;
		}
		
		
		Runnable runnable = new Runnable() {
	        public void run() {
	        	stopSelf();
	        }
	    };
		
	    private Handler handlerData = new Handler(Looper.getMainLooper());
		
		private void pollForUpdates() {
			timer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					
						//System.out.println("Entered Timer .......................");
						
						if(!isNotEntered) {
							//System.out.println("I am going to return manual timer ....................");
							return;
						}
						
							if(locationManager == null) {
								getLocations();
							}
							
							if(locationManager != null) {
								boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
								
								if(isGPSEnabled) {
									
									//System.out.println("Only once i will come for 20 seconds manual timer");
									
									
									if(isNotEntered) {
										isNotEntered = false;
										try {
											handlerData.post(new Runnable() {
											      public void run() {
											          new ManualLocationChangeTask().execute("");
											      }
											   });
										} catch (Exception e) {
											isNotEntered = true;
											e.printStackTrace();
										}
									}
									
								}
							}
						
					
					
				}
			}, 0, manualPollUpdatesTime);
			
		}
		
		
		
		
		class ManualLocationChangeTask extends AsyncTask<String, String, String> {  


		    protected String doInBackground(String... urls) {
		    	
		    	isNotEntered = false;
		    	
		    	getLocationManual();
		    	
		    	isNotEntered = true;
		    	
		        return "";
		    }

		    protected void onPostExecute(String data) {
		    	isNotEntered = true;
		    }
		}
		
		
		
	
	public void getLocationManual() {
		
		
		String locationProvider = LocationManager.GPS_PROVIDER;
		
		Location lastKnownLocation = null;
		
		if(locationManager != null) {
			lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
		}
		
		//System.out.println(GPSService.this.distance+" *************************************************************");
		
		if(lastKnownLocation != null) {
			
			SharedPreferences data = getSharedPreferences("locationdata", Context.MODE_WORLD_READABLE);
		    double lat = Double.parseDouble(data.getString("latitude", lastKnownLocation.getLatitude()+""));
		    double lon = Double.parseDouble(data.getString("longitude", lastKnownLocation.getLongitude()+""));
				
		    double distance = findDistance(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude(), lat, lon);
		  
		    curLocation = lastKnownLocation;
	        
	        lat = curLocation.getLatitude();
	        lon = curLocation.getLongitude();  
	        
			SharedPreferences.Editor editor = data.edit();
			editor.putString("latitude", lat+"");
			editor.putString("longitude", lon+"");
			editor.commit();
			String currentDateTimeString = utilDateFormatter.format(new Date());
	        
	        try {
	        	
	        	
		        	Intent battery= getApplicationContext().registerReceiver(null,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
			        int level = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
			        int scale = battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
			        batteryPct = level / (float)scale *100;
	        	  
		        	
		    		//String urlStr = "http://"+ipid+"/vehicleservice/andromobium?latitude="+curLocation.getLatitude()+"&longitude="+curLocation.getLongitude()+"&deviceid="+deviceid+"&datetime="+currentDateTimeString+"&battery="+batteryPct+"&distance="+distance+"";
					String urlStr = "http://"+ipid+"/childface/andromobium?latitude="+curLocation.getLatitude()+"&longitude="+curLocation.getLongitude()+"&deviceid="+deviceid+"&datetime="+currentDateTimeString+"&battery="+batteryPct+"&distance="+distance+"";

					urlStr = urlStr.toString().replace(" ", "%20");
		      		URL url = new URL(urlStr);
		      		
		      		
		    		HttpURLConnection deviceData = (HttpURLConnection) url.openConnection();
		    		deviceData.setRequestMethod("POST");
		    		deviceData.setConnectTimeout(45000);
		      		
		    		BufferedReader in = new BufferedReader(new InputStreamReader( deviceData.getInputStream()));
		    		String inputLine;
		      		
	    			while ((inputLine = in.readLine()) != null) {
	    			   
	    			}
	    			
	    			in.close();
		    			    		
			      	
	    			
	        } catch (Exception e) {
	        	
	        	try {
	      		
					dbHandler.addBackupData(curLocation.getLatitude(), curLocation.getLongitude(), deviceid, currentDateTimeString, distance, batteryPct);
				
				} catch (Exception e1) { 
					
				}
			}
	        
        
       
		} else {
	        if(curLocation != null) {
	           
	            SharedPreferences data = getSharedPreferences("locationdata", Context.MODE_WORLD_READABLE);
	            double lat = Double.parseDouble(data.getString("latitude",  curLocation.getLatitude()+""));
	            double lon = Double.parseDouble(data.getString("longitude", curLocation.getLongitude()+""));
	               
	            double distance = findDistance(curLocation.getLatitude(), curLocation.getLongitude(), lat, lon);
	           
	           
	            lat = curLocation.getLatitude();
	            lon = curLocation.getLongitude();
	           
	            
	            SharedPreferences.Editor editor = data.edit();
	           
	            editor.putString("latitude", lat+"");
	            editor.putString("longitude", lon+"");
	                       
	            editor.commit();
	           
	           
	            String currentDateTimeString = utilDateFormatter.format(new Date());
	           
	            try {
	            	
	            	
	            	Intent battery= getApplicationContext().registerReceiver(null,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
			        int level = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
			        int scale = battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
			        batteryPct = level / (float)scale *100;
	        	    
	               /* String urlStr = "http://"+ipid+"/vehicleservice/andromobium?latitude="+curLocation.getLatitude()+"&longitude="+curLocation.getLongitude()+"&deviceid="+deviceid+"&datetime="+currentDateTimeString+"&battery="+batteryPct+"&distance="+distance+"";*/
					String urlStr = "http://"+ipid+"/childface/andromobium?latitude="+curLocation.getLatitude()+"&longitude="+curLocation.getLongitude()+"&deviceid="+deviceid+"&datetime="+currentDateTimeString+"&battery="+batteryPct+"&distance="+distance+"";

					urlStr = urlStr.toString().replace(" ", "%20");
	                URL url = new URL(urlStr);
	              
	                
	                
	                HttpURLConnection deviceData = (HttpURLConnection) url.openConnection();
		    		deviceData.setRequestMethod("POST");
		    		deviceData.setConnectTimeout(45000);
		      		
		    		BufferedReader in = new BufferedReader(new InputStreamReader( deviceData.getInputStream()));
		    		String inputLine;
		      		
	    			while ((inputLine = in.readLine()) != null) {
	    			    
	    			}
	    			
	    			in.close();
	                
	                
	            } catch (Exception e) {
	               
	            	try {
	                  dbHandler.addBackupData(curLocation.getLatitude(), curLocation.getLongitude(), deviceid, currentDateTimeString, distance, batteryPct);
	                } catch (Exception e1) {
	                
	                }
	            }
	         
	           
	        }
      
		}
		
		
		try {
			
			
			
			int countValue = dbHandler.getCountData();  
    		
    		for(int x = 0; x < countValue; x++) {
    			
    			String text = dbHandler.getChunkData();
    				       	
	    		
		      	if(text != null && text.length() > 0) { 
		       		
		       		String urlParameters = "backupdata="+text; 
			      /* 	String urlStrBackup = "http://"+ipid+"/vehicleservice/andromobium";*/
					String urlStrBackup = "http://"+ipid+"/childface/andromobium";
			       	urlParameters = urlParameters.toString().replace(" ", "%20");
		      		URL urlBackup = new URL(urlStrBackup);
		      		
		      		
		      		HttpURLConnection ycBackup = (HttpURLConnection) urlBackup.openConnection();
		      		ycBackup.setRequestMethod("POST");
		      		ycBackup.setConnectTimeout(45000);
		      		ycBackup.setRequestProperty("Content-Length", "" + 
		                     Integer.toString(urlParameters.getBytes().length));
		      		ycBackup.setUseCaches (false);
		      		ycBackup.setDoInput(true);
		      		ycBackup.setDoOutput(true);
		      		
		      		//Send request
		            DataOutputStream wr = new DataOutputStream (ycBackup.getOutputStream ());
		            wr.writeBytes (urlParameters);
		            wr.flush ();
		            wr.close ();
		         
		          //Get Response	
		            InputStream is = ycBackup.getInputStream(); 
		            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		            String line;
		            //StringBuffer response = new StringBuffer(); 
		            while((line = rd.readLine()) != null) {
		             
		            }
		            rd.close();
		           
		            dbHandler.deleteChunkData();
		    
		       	}
    	    }
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		

		
		
	}
	
	

	public void getLocations() {
		
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationListener = new MyLocationListener();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, time, distance,locationListener);
		
	}
	
	
	public double findDistance(double lat, double lon, double currentlat, double currentlon) {
		
		double dist = 0.0; 
        double deltaLat = Math.toRadians(currentlat - lat); 
        double deltaLon = Math.toRadians(currentlon - lon); 
        lat = Math.toRadians(lat); 
        currentlat = Math.toRadians(currentlat); 
        lon = Math.toRadians(lon); 
        currentlon = Math.toRadians(currentlon); 
        double earthRadius = 6371; 
        double a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) + 
        Math.cos(lat) * Math.cos(currentlat) * Math.sin(deltaLon/2) * Math.sin(deltaLon/2); 
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        dist = earthRadius * c; 
		return dist;
		 
	}
	
	boolean isNotEntered = true;


	class LocationChangeTask extends AsyncTask<Location, String, String> {


	    protected String doInBackground(Location... urls) {
	    	
	    	//isNotEntered = false;
	    	
	    	Location location = urls[0];
	    	
	    	String currentDateTimeString = utilDateFormatter.format(new Date());
			
			try {
			
		        if(curLocation == null) {
		              curLocation = location;
		              locationChanged = true;
		        } 
		        
		        
		        
		       
		        SharedPreferences data = getSharedPreferences("locationdata", Context.MODE_WORLD_READABLE);
		        double lat = Double.parseDouble(data.getString("latitude", location.getLatitude()+""));
		        double lon = Double.parseDouble(data.getString("longitude", location.getLongitude()+""));
				        
		        double distance = findDistance(location.getLatitude(), location.getLongitude(), lat, lon);
		               
		        curLocation = location; 
		      
				currentDateTimeString = utilDateFormatter.format(new Date());
				 
	        	double distanceMetres = distance*1000;
	        	
	        	//System.out.println(GPSService.this.distance+" Sending data "+distanceMetres);
	        	
	        	if(distanceMetres >= GPSService.this.distance) {
	        		
	        		//System.out.println("Sending data "+distanceMetres);
	        		
			        		lat = curLocation.getLatitude();
			    	        lon = curLocation.getLongitude();
			    	        
			    	        
			    			SharedPreferences.Editor editor = data.edit();
			    			
			    			editor.putString("latitude", lat+"");
			    			editor.putString("longitude", lon+"");
			    						
			    			editor.commit();
	        	
				        	Intent battery= getApplicationContext().registerReceiver(null,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
					        int level = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
					        int scale = battery.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
					        batteryPct = level / (float)scale *100;
			        	    
				    		String urlStr = "http://"+ipid+"/childface/andromobium?latitude="+curLocation.getLatitude()+"&longitude="+curLocation.getLongitude()+"&deviceid="+deviceid+"&datetime="+currentDateTimeString+"&battery="+batteryPct+"&distance="+distance+"";
				    		urlStr = urlStr.toString().replace(" ", "%20");
				      		URL url = new URL(urlStr);
				      		
				    		HttpURLConnection deviceData = (HttpURLConnection) url.openConnection();
					    	deviceData.setRequestMethod("POST");
					    	deviceData.setConnectTimeout(45000);
					      		
					    	BufferedReader in = new BufferedReader(new InputStreamReader( deviceData.getInputStream()));
					    	String inputLine;
					      		
				    		while ((inputLine = in.readLine()) != null) {
				    			   
				    		}
				    			
				    		in.close();
				    		
				    		//System.out.println("sent data");
				            
	        	}
	        	
	        	//System.out.println("Enetered location changed *****************");
	        	
	    		
	        	if(!isNotEntered) {
					//System.out.println("I am going to return ....................");
					return "";
	        	}
			
	        	
	        	
	        	if(isNotEntered) {
	        		
	        		isNotEntered = false;
	        	
				        	try {
				    			
				    			int countValue = dbHandler.getCountData();  
				        		
				        		for(int x = 0; x < countValue; x++) {
				        			
				        			String text = dbHandler.getChunkData();
				        				       	
				    	    		
				    		      	if(text != null && text.length() > 0) { 
				    		       		
				    		       		String urlParameters = "backupdata="+text; 
				    			       	//String urlStrBackup = "http://"+ipid+"/vehicleservice/andromobium";
										String urlStrBackup = "http://"+ipid+"/childface/andromobium";
										urlParameters = urlParameters.toString().replace(" ", "%20");
				    		      		URL urlBackup = new URL(urlStrBackup);
				    		      		
				    		      		
				    		      		HttpURLConnection ycBackup = (HttpURLConnection) urlBackup.openConnection();
				    		      		ycBackup.setRequestMethod("POST");
				    		      		ycBackup.setConnectTimeout(45000);
				    		      		ycBackup.setRequestProperty("Content-Length", "" + 
				    		                     Integer.toString(urlParameters.getBytes().length));
				    		      		ycBackup.setUseCaches (false);
				    		      		ycBackup.setDoInput(true);
				    		      		ycBackup.setDoOutput(true);
				    		      		
				    		      		//Send request
				    		            DataOutputStream wr = new DataOutputStream (ycBackup.getOutputStream ());
				    		            wr.writeBytes (urlParameters);
				    		            wr.flush ();
				    		            wr.close ();
				    		            
				    		          //Get Response	
				    		            InputStream is = ycBackup.getInputStream();
				    		            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
				    		            String line;
				    		            //StringBuffer response = new StringBuffer(); 
				    		            while((line = rd.readLine()) != null) {
				    		             
				    		            }
				    		            rd.close();
				    		           
				    		            dbHandler.deleteChunkData();
				    		    
				    		       	}
				        	    }
				    		} catch(Exception e) {
				    			e.printStackTrace();
				    		}
	        		}
	        	
	        	
	       	  
    			
	      	} catch (Exception e) {
	      		
	      		e.printStackTrace();
	      		try {
	      			
	      			dbHandler.addBackupData(curLocation.getLatitude(), curLocation.getLongitude(), deviceid, currentDateTimeString, distance, batteryPct);
					
				} catch (Exception e1) { 
					e1.printStackTrace();
				}	      		
	      	}
	    	
			isNotEntered = true;
	    	
	        return "";
	    }

	    protected void onPostExecute(String data) {
	    	isNotEntered = true;
	    }
	}
	
	
	
	
	private class MyLocationListener implements LocationListener {
		
		public void onLocationChanged(Location location) {
			
			
			/*if(!isNotEntered) {
				//System.out.println("I am going to return ....................");
				return;
			}*/
			
			//System.out.println("Only once i will come for 20 seconds");
			
			
			/*if(isNotEntered) {
				isNotEntered = false;*/	
				try {
					LocationChangeTask locationChangeTask = new LocationChangeTask();	
					locationChangeTask.execute(location);
				} catch (Exception e) {
					//isNotEntered = true;
					e.printStackTrace();
				}
				
			//}
			 
		}

		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
			try {
				Date date = new Date();
				DateFormat utilDateFormatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
				String currentDateTimeString = utilDateFormatter.format(date);
				dbHandler.addData("OFF", currentDateTimeString, "GPS");
			} catch (Exception e) {
				
			}
		}

		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
			try {
				Date date = new Date();
				DateFormat utilDateFormatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
				String currentDateTimeString = utilDateFormatter.format(date);
				dbHandler.addData("ON", currentDateTimeString, "GPS");
			} catch (Exception e) {
				
			}
			
		
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
		}
	}
	
	
	
	
	final class MailTask extends AsyncTask<String, Void, String> {
		
  		
          @Override
          protected String doInBackground(String... urls) {
             try {
            	 	DatabaseHandler dbHandler = new DatabaseHandler(GPSService.this);
        			SharedPreferences data = getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
        			String deviceId = data.getString("deviceid", null);       				
        			String text = dbHandler.getData();
        			//System.out.println(text.length());
        			if(text.length() > 0) {			    
	        	        GMailSender sender = new GMailSender("quadriviumtracking@gmail.com", "quad@123");
	        	        sender.sendMail("GPS AND INTERNET REPORTS OF "+deviceId,   
	        	                		 text,   
	        	                         "quadriviumtracking@gmail.com","nalgondareport@gmail.com");   
	        	        dbHandler.deleteData();
	        	        return "";
	        	        
        			} else {
        				return "";
        			}
        	       
             } catch(Exception e) {
            	 
            	 return "EXCEPTION"; 
             }
        	  
          }   
          
   
          @Override
          protected void onPostExecute(String output) {
        	  
        	  if(!(output.contains("EXCEPTION"))) {
	        	  SharedPreferences.Editor editor = data.edit();
				  editor.putBoolean("requestemaillog", false);
				  editor.commit();
        	  }
          	 
          }
  	    }

}
