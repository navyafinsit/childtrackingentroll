package com.childfacebiometrics;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.childfacebiometrics.SampleDialogFragment.SampleDialogListener;

//import com.childfacebiometrics.CrimeReportActivity.UploadTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FaceScanActivity extends Activity implements SampleDialogListener,View.OnClickListener,DialogInterface.OnClickListener {
	/** Called when the activity is first created. */
	private static Button mButtonScan;
	private static Button mButtonStop;
	private Button mButtonSave;
	private static TextView mMessage;
	private static TextView mScannerInfo;
	private static ImageView mFingerImage;
	private CheckBox mCheckFrame;
	private CheckBox mCheckLFD;
	private CheckBox mCheckInvertImage;
	private CheckBox mCheckUsbHostMode;

	public static boolean mStop = false;
	public static boolean mFrame = true;
	public static boolean mLFD = false;
	public static boolean mInvertImage = false;

	public static final int MESSAGE_SHOW_MSG = 1;
	public static final int MESSAGE_SHOW_SCANNER_INFO = 2;
	public static final int MESSAGE_SHOW_IMAGE = 3;
	public static final int MESSAGE_ERROR = 4;
	public static final int MESSAGE_TRACE = 5;

	public static byte[] mImageFP = null;
	public static Object mSyncObj= new Object();

	public static int mImageWidth = 0;
	public static int mImageHeight = 0;
	private static int[] mPixels = null;
	private static Bitmap mBitmapFP = null;
	private static Canvas mCanvas = null;
	private static Paint mPaint = null;

	//private FPScan mFPScan = null;
	//
	public static boolean mUsbHostMode = true;

	// Intent request codes
	private static final int REQUEST_FILE_FORMAT = 1;
	//private UsbDeviceDataExchangeImpl usb_host_ctx = null;

	//ADDED BY DINESH
	DialogFragment serverProcessDialogFragment;
	Context myContext;
	SharedPreferences data;
	String criminalName;
	String templateURL;
	String address;
	String phoneNo;
	String deviceId;
	String latitude;
	String longtitude;
	String personCategory;
	String webImage;
	Bitmap imgBitMap;
	private Context context;

	ArrayList<TinDetailsBean> tinResults=new ArrayList<TinDetailsBean>();
	Uri mImageUri;
    AlertDialog ad=null;
	Button saveBtn=null;
	Button cancelBtn=null;
	TextView pcNo=null;
	static String pcnumber="";
	//saveBtn.setEnabled(false);
	TextView errorText;

	public static void InitFingerPictureParameters(int wight, int height)
	{
		mImageWidth = wight;
		mImageHeight = height;

		mImageFP = new byte[FaceScanActivity.mImageWidth*FaceScanActivity.mImageHeight];
		mPixels = new int[FaceScanActivity.mImageWidth*FaceScanActivity.mImageHeight];

		mBitmapFP = Bitmap.createBitmap(wight, height, Config.RGB_565);

		mCanvas = new Canvas(mBitmapFP);
		mPaint = new Paint();

		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0);
		ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
		mPaint.setColorFilter(f);


	}

	//added by dinesh
	Button moreButton;



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.facemain);
		mFrame = true;
		mUsbHostMode = true;
		mLFD = false;
		mInvertImage=true;
		getActionBar().setDisplayShowHomeEnabled(true);
		mButtonScan = (Button) findViewById(R.id.btnScan);
		mButtonStop = (Button) findViewById(R.id.btnStop);
		mButtonSave = (Button) findViewById(R.id.btnFaceSave);
		mMessage = (TextView) findViewById(R.id.tvMessage);
		mScannerInfo = (TextView) findViewById(R.id.tvScannerInfo);
		mFingerImage = (ImageView) findViewById(R.id.imageFinger);
		mCheckFrame = (CheckBox) findViewById(R.id.cbFrame);
		mCheckLFD = (CheckBox) findViewById(R.id.cbLFD);
		mCheckInvertImage = (CheckBox) findViewById(R.id.cbInvertImage);
		mCheckUsbHostMode = (CheckBox) findViewById(R.id.cbUsbHostMode);
		this.context=getApplicationContext();

		//added by dinesh
		try {
			myContext = createPackageContext("com.childfacebiometrics",Context.CONTEXT_IGNORE_SECURITY);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			Toast.makeText(getApplicationContext(), "Please Install PCATS Application",Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		moreButton=(Button)findViewById(R.id.button1);
		moreButton.setEnabled(false);
		moreButton.setBackgroundResource(R.drawable.custom_button_grey);
		moreButton.setOnClickListener(this);

		data = myContext.getSharedPreferences("datastore", Context.MODE_WORLD_READABLE);
		deviceId=data.getString("deviceid",null);
		/*if(deviceId==null)
		{
			deviceId="OG1001B1";
		}*/

		//usb_host_ctx = new UsbDeviceDataExchangeImpl(this, mHandler);



		//added by dinesh for camera photo image // UNCOMMENT THIS FOR CAMERA

		mButtonScan.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				FaceScanActivity.InitFingerPictureParameters(320,480);



			/*	Camera camera= Camera.open();
				Camera.Parameters param;
				param = camera.getParameters();

				Camera.Size bestSize = null;
				List<Camera.Size> sizeList = camera.getParameters().getSupportedPreviewSizes();
				bestSize = sizeList.get(0);
				for(int i = 1; i < sizeList.size(); i++){
					if((sizeList.get(i).width * sizeList.get(i).height) > (bestSize.width * bestSize.height)){
						bestSize = sizeList.get(i);
					}
				}

				List<Integer> supportedPreviewFormats = param.getSupportedPreviewFormats();
				Iterator<Integer> supportedPreviewFormatsIterator = supportedPreviewFormats.iterator();
				while(supportedPreviewFormatsIterator.hasNext()){
					Integer previewFormat =supportedPreviewFormatsIterator.next();
					if (previewFormat == ImageFormat.YV12) {
						param.setPreviewFormat(previewFormat);
					}
				}

				param.setPreviewSize(bestSize.width, bestSize.height);

				param.setPictureSize(bestSize.width, bestSize.height);

				camera.setParameters(param);
				//camera.takePicture(shutterCallback, rawCallback, jpegCallback);
*/

				//added code by dinesh to take original image
				Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
				File photo;
				try
				{
					// place where to store camera taken picture

					File tempDir= Environment.getExternalStorageDirectory();
					tempDir=new File(tempDir.getAbsolutePath()+"/.temp/");
					if(!tempDir.exists())
					{
						tempDir.mkdirs();
					}
					photo=File.createTempFile("picture", ".jpg", tempDir);
					photo.delete();
				}
				catch(Exception e)
				{

					Toast.makeText(getApplicationContext(), "Please check SD card! Image shot is impossible!", Toast.LENGTH_LONG).show();
					return;
				}
				mImageUri = Uri.fromFile(photo);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);

				//start camera intent
				//activity.startActivityForResult(this, intent, MenuShootImage);



				//end of added code by dinesh


				ActivityList.setCurActivity(ActivityList.CAMERA);

				//intent.putExtra("outputX", 768);
				//intent.putExtra("outputY", 1024);
				//intent.putExtra("crop", "true");
				//Camera camera= Camera.open();
				//camera.setDisplayOrientation(90);
				startActivityForResult(intent, 0);

			}

		});
		//end of added code by dinesh.

		//method added by dinesh from StartPage



		//commented by dinesh for camera interation. // UNCOMMENT USE THIS FOR SCANNER
      /*  mButtonScan.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
	        		if( mFPScan != null )
	        		{
	        			mStop = true;
	        			mFPScan.stop();

	        		}
	        		mStop = false;
	        		if(mUsbHostMode)
	        		{
		        		usb_host_ctx.CloseDevice();
		        		if(usb_host_ctx.OpenDevice(0, true))
		                {
		        			if( StartScan() )
			        		{
			        			mButtonScan.setEnabled(false);
			        	        mButtonSave.setEnabled(false);
			        	        mCheckUsbHostMode.setEnabled(false);
			        	        mButtonStop.setEnabled(true);
			        		}
		                }
		            	else
		            	{
		            		if(!usb_host_ctx.IsPendingOpen())
		            		{
		            			//mMessage.setText("Can not start scan operation.\nCan't open scanner device");
								mMessage.setText("Scanner not detected.\n Please check your OTG cable");
		            		}
		            	}
	        		}
	        		else
	        		{
	        			if( StartScan() )
		        		{
		        			mButtonScan.setEnabled(false);
		        	        mButtonSave.setEnabled(false);
		        	        mCheckUsbHostMode.setEnabled(false);
		        	        mButtonStop.setEnabled(true);
		        		}
	        		}
        		}
        });
*/

	/*	mButtonStop.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mStop = true;
				if( mFPScan != null )
				{
					mFPScan.stop();
					mFPScan = null;

				}
				mButtonScan.setEnabled(true);
				mButtonSave.setEnabled(true);
				mCheckUsbHostMode.setEnabled(true);
				mButtonStop.setEnabled(false);
			}
		});*/


			mButtonStop.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Intent i=new Intent(FaceScanActivity.this,EnrollFaceActivity.class);
				startActivity(i);
				finish();
			}
		});


		final LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);




		mButtonSave.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if( mImageFP != null){
					//SaveImage();
				/*	View layout = inflater.inflate(R.layout.pcnodialog,(ViewGroup) findViewById(R.id.pcdialog_layout_root));

					saveBtn = (Button) layout.findViewById(R.id.save);
					cancelBtn=(Button) layout.findViewById(R.id.cancel);
					pcNo=(TextView)layout.findViewById(R.id.deviceid);
					//saveBtn.setEnabled(false);


					errorText = (TextView) layout.findViewById(R.id.errortext);
					cancelBtn.setOnClickListener(new View.OnClickListener(){

						public void onClick(View v) {

							if(ad!=null)
								ad.cancel();
						}

					});

					saveBtn.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {

							if(pcNo==null || pcNo.length()<4)
							{
								errorText.setText("Please Enter ID CARD Number");
							}else
							{
								pcnumber=pcNo.getText()+"";
								if(ad!=null)
								{
									ad.cancel();
								}
								OnBtnVerifyIso(); // CAMERA
							}


						}

					});

				// kdkkkkk
					AlertDialog.Builder adb = new AlertDialog.Builder(FaceScanActivity.this);
					adb.setTitle("Enter ID CARD Number");
					adb.setView(layout);
					adb.setCancelable(false);
					ad=adb.create();
                    ad.show();
*/
					OnBtnVerifyIso(); // CAMERA

										   }


				//kkkkkkkk






				//OnBtnVerifyIsoScanner(); //SCANNER
			}
		});


		mCheckFrame.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (buttonView.isChecked())
					mFrame = true;
				else
				{
					mFrame = false;
					mCheckLFD.setChecked(false);
					mLFD = false;
				}
			}
		});


		mCheckLFD.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (buttonView.isChecked())
					mLFD = true;
				else
					mLFD = false;
			}
		});

		mCheckInvertImage.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (buttonView.isChecked())
					mInvertImage = true;
				else
					mInvertImage = false;
			}
		});

		mCheckUsbHostMode.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (buttonView.isChecked())
					mUsbHostMode = true;
				else
					mUsbHostMode = false;
			}
		});

		//ADDED BY DINESH
		serverProcessDialogFragment=new SampleDialogFragment();
	}



	//method added by dinesh for camera (moved from StartPage)
	@Override
	public void onActivityResult(final int requestCode, final int resultCode,
								 final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);



		if (resultCode == RESULT_CANCELED) {
			Toast.makeText(FaceScanActivity.this, "Image Is Canclelled",Toast.LENGTH_LONG).show();
		}else if (requestCode == 0 && resultCode == RESULT_OK)
		{
			Toast.makeText(FaceScanActivity.this, "Image Is captured",Toast.LENGTH_LONG).show();



			this.getContentResolver().notifyChange(mImageUri, null);
			ContentResolver cr = this.getContentResolver();
			Bitmap bitmap=null;
			Bitmap rotatedImg=null;
			try
			{
				bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);

				Matrix matrix = new Matrix();
				matrix.postRotate(0);
				rotatedImg = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

				rotatedImg=rotateImageIfRequired(rotatedImg,getApplicationContext(),mImageUri);


			}
			catch (Exception e)
			{
				Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
				//Log.d(TAG, "Failed to load", e);
			}

			/*Bundle extras = data.getExtras();
			Bitmap b = (Bitmap) extras.get("data");
			if(b==null){
				System.out.println("PLEASE TAKE THE PICTURE");
				//Toast.makeText(StartPage.this, "Please capture the image",Toast.LENGTH_LONG).show();
				return;
			}
*/
			//mCanvas.setBitmap(b);


			rotatedImg=resizeBitmapFitXY(320,480,rotatedImg);


			mFingerImage.setImageBitmap(rotatedImg);

			mFingerImage.invalidate();
			imgBitMap=rotatedImg;

			//ByteArrayOutputStream stream = new ByteArrayOutputStream();
			//b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			//byte[] byteArray = stream.toByteArray();
			//System.out.println(byteArray.length);
			//mImageFP=byteArray;
			FaceScanActivity.fingerPrintImageBMP=imgBitMap;
			mButtonSave.setEnabled(true);
			mButtonStop.setEnabled(true);
		}

		/*
		Thread t = new Thread() {
			public void run() {

				*//*		if (resultCode == RESULT_CANCELED) {
							//showToast("camera cancelled", 10000);
							return;
						}
						// lets check if we are really dealing with a picture
						if (requestCode == 0 && resultCode == RESULT_OK) {
							Bundle extras = data.getExtras();
							Bitmap b = (Bitmap) extras.get("data");
							if(b==null){
								System.out.println("PLEASE TAKE THE PICTURE");
								//Toast.makeText(StartPage.this, "Please capture the image",Toast.LENGTH_LONG).show();
								return;
							}
							// setContentView(R.layout.startpage);
							File sdCard = Environment.getExternalStorageDirectory();
							File file = new File(sdCard, fileName);
							System.out.println("*-*-*-*file name is-*-*-*--"+ fileName);
							FileOutputStream fos = null;
							try {
								file.createNewFile();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							try {
								fos = new FileOutputStream(file);

							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							b.compress(Bitmap.CompressFormat.JPEG, 95, fos);
							String timestamp = Long.toString(System.currentTimeMillis());
							MediaStore.Images.Media.insertImage(getContentResolver(), b, timestamp, timestamp);
							fileName = "/storage/sdcard0/" + fileName;

*//*


			}

		};
		t.start();*/
	}


	//end of added method by dinesh.

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}



	private void SaveImage()
	{
		Intent serverIntent = new Intent(this, SelectFileFormatActivity.class);
		startActivityForResult(serverIntent, REQUEST_FILE_FORMAT);
	}





	private static void ShowBitmap()
	{
		for( int i=0; i<mImageWidth * mImageHeight; i++)
		{
			mPixels[i] = Color.rgb(mImageFP[i],mImageFP[i],mImageFP[i]);
		}

		mCanvas.drawBitmap(mPixels, 0, mImageWidth, 0, 0,  mImageWidth, mImageHeight, false, mPaint);

		mFingerImage.setImageBitmap(mBitmapFP);
		mFingerImage.invalidate();

		synchronized (mSyncObj)
		{
			mSyncObj.notifyAll();
		}


	}

   /* public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
         case REQUEST_FILE_FORMAT:
			 if (resultCode == Activity.RESULT_OK) {
			     // Get the file format
				 String[] extraString = data.getExtras().getStringArray(SelectFileFormatActivity.EXTRA_FILE_FORMAT);
				 String fileFormat = extraString[0];
				 String fileName = extraString[1];
				 SaveImageByFileFormat(fileFormat, fileName);
             }
			 else
				 mMessage.setText("Cancelled!");
             break;
        }
    }*/

	//ADDED BY DINESH FOR ALERT BOX..
	public void alertboxToConfigure(String title, final String msg) {
		if (msg != null) {
			new AlertDialog.Builder(this)
					.setMessage(msg)
					.setTitle(title)
					.setCancelable(true)
					.setNeutralButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton){
									/*if(msg.contains("Device Configured Successfully"))
									{
										Intent i=new Intent(TestgpsActivity.this,FtrScanDemoUsbHostActivity.class);
										startActivity(i);
										finish();
									}*/
		    		/*  else
		    		  {
		    			  Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
		    			  return;
		    		  }*/
								}
							}).show();
		}
	}


	public void OnBtnVerifyIsoScanner(){
		//Toast.makeText(getApplicationContext(), img_fp_src.getDrawable(), 5).show();
		try{

			//futronics code
			MyBitmapFile fileBMP = new MyBitmapFile(mImageWidth, mImageHeight, mImageFP);
			Bitmap bitmap = BitmapFactory.decodeByteArray(fileBMP.toBytes() , 0, fileBMP.toBytes().length);

			if(bitmap!=null){
				System.out.println("Entered into image save option");
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
				bitmap.setDensity(500);

				byte[] imageInByte = stream.toByteArray();
				//Toast.makeText(CrimeReportActivity.this, "Image attached", Toast.LENGTH_LONG).show();
				//ByteArrayInputStream bis = new ByteArrayInputStream(imageInByte);

				// added by dinesh for IMAGE INTERNAL MEMORY
				ContextWrapper cw = new ContextWrapper(getApplicationContext());
				// path to /data/data/yourapp/app_data/imageDir
				File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
				// Create imageDir
				File mypath=new File(directory,"template.dat");


				FileOutputStream fos = new FileOutputStream(mypath.getAbsolutePath());

				//FileOutputStream fos = new FileOutputStream("/storage/sdcard0/template.dat");
				fos.write(imageInByte);
				fos.close();
				serverProcessDialogFragment.show(getFragmentManager(), "DIALOG_TYPE_PROGRESS");
				serverProcessDialogFragment.setCancelable(false);

				UploadFaceTask task=new UploadFaceTask();
				//task.execute("/storage/sdcard0/template.dat");
				task.execute(mypath.getAbsolutePath());
				//tvInfo.setText(output);
				//Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();



			}else{
				return;
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	/* **********************************************************************
     * METHODS FROM OTHER CLASSES
     *
     *
     * **********************************************************************
     */
	public void OnBtnVerifyIso(){
		//Toast.makeText(getApplicationContext(), img_fp_src.getDrawable(), 5).show();
		try{

			//futronics code
    	/*MyBitmapFile fileBMP = new MyBitmapFile(mImageWidth, mImageHeight, mImageFP);
      	Bitmap bitmap = BitmapFactory.decodeByteArray(fileBMP.toBytes() , 0, fileBMP.toBytes().length);*/
			//Bitmap bitmap=imgBitMap;

			/*ByteArrayOutputStream str = new ByteArrayOutputStream();

			imgBitMap.compress(Bitmap.CompressFormat.JPEG,100,str);
			byte[] byteArray = str.toByteArray();

			Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
*/
			//Bitmap bitmap=Bitmap.createScaledBitmap(bmp, 320, 480, false);
			Bitmap bitmap=imgBitMap;

			//Bitmap bitmap=imgBitMap;
			//bitmap=Bitmap.createScaledBitmap(imgBitMap, 768, 1024, false);

			if(bitmap!=null){
				System.out.println("Entered into image save option");
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				//bitmap.setDensity(500);

				byte[] imageInByte = stream.toByteArray();
				//Toast.makeText(CrimeReportActivity.this, "Image attached", Toast.LENGTH_LONG).show();
				//ByteArrayInputStream bis = new ByteArrayInputStream(imageInByte);

				// added by dinesh for IMAGE INTERNAL MEMORY
				ContextWrapper cw = new ContextWrapper(getApplicationContext());
				// path to /data/data/yourapp/app_data/imageDir
				File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
				// Create imageDir
				File mypath=new File(directory,"template.dat");


				FileOutputStream fos = new FileOutputStream(mypath.getAbsolutePath());

				//FileOutputStream fos = new FileOutputStream("/storage/sdcard0/template.dat");
				fos.write(imageInByte);
				fos.close();
				serverProcessDialogFragment.show(getFragmentManager(), "DIALOG_TYPE_PROGRESS");
				serverProcessDialogFragment.setCancelable(false);

				UploadFaceTask task=new UploadFaceTask();
				//task.execute("/storage/sdcard0/template.dat");
				task.execute(mypath.getAbsolutePath());
				//tvInfo.setText(output);
				//Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();



			}else{
				return;
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private class UploadFaceTask extends AsyncTask<String, Void, String> {
		String output=null;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... urls) {
			for (String url : urls) {
				output = upload(url);
				//output = "";


			}
	       /* if(output!=null){
	        	onPostExecute(output);
	        }*/
			return output;
		}




		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//String criminalName = null;
			//final ImageView imageView = (ImageView) findViewById(R.id.criminalImage);
			//final TextView nameOfTheCriminal=(TextView) findViewById(R.id.criminalName);
			System.out.println("Entered into on post executes"+result);
			moreButton.setVisibility(android.view.View.VISIBLE);

			//added to check if the device is authorized to communicate to the server

			if(result!=null && "".equals(result.trim()))
			{
				//tvInfo.setText("NO MATCHES FOUND");
				moreButton.setEnabled(true);
				moreButton.setText("No Match. Enroll?");
				moreButton.setBackgroundColor(Color.RED);

				serverProcessDialogFragment.dismiss();

				return;
			}else
			{
				serverProcessDialogFragment.dismiss();

				if("DISABLED".equalsIgnoreCase(result.trim()))
				{
					//Context context = getApplicationContext();
				/*	System.out.println("** DISABLED **");
					CharSequence text = "YOUR DEVICE IS DISABLED";
					int duration = Toast.LENGTH_LONG;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();*/

					alertboxToConfigure("CONFIGURATION VOILATION",  "YOUR DEVICE IS DISABLED");

					return;
				}

				if("UNAUTHORIZED".equalsIgnoreCase(result.trim()))
				{
					/*//Context context = getApplicationContext();
					CharSequence text = "YOUR DEVICE IS NOT AUTHORIZED";
					int duration = Toast.LENGTH_LONG;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();*/
					alertboxToConfigure("CONFIGURATION VOILATION",  "YOUR DEVICE IS NOT AUTHORIZED");
					return;
				}

				if("OBJECT_NOT_FOUND".equalsIgnoreCase(result.trim()))
				{
					alertboxToConfigure("BAD PICTURE QUALITY",  "PLEASE TAKE A PICTURE AGAIN");

					return;
				}
				if("WRONG_PC_NUMBER".equalsIgnoreCase(result.trim()))
				{
					alertboxToConfigure("ID CARD NO NOT MATCHING",  "PC PHOTO ALREADY IN DATABASE");

					return;
				}
				if("MATCH_NOT_FOUND".equalsIgnoreCase(result.trim()))
				{
					moreButton.setEnabled(true);
					moreButton.setText("No Match. Enroll?");
					moreButton.setBackgroundColor(Color.RED);
					alertboxToConfigure("FACE NOT IN DATABASE",  "PLEASE ENROLL");

					return;
				}

				if("PC_NUMBER_NOT_EXIST_IN_DATABASE".equalsIgnoreCase(result.trim()))
				{

					moreButton.setEnabled(true);
					moreButton.setText("No Match. Enroll?");
					moreButton.setBackgroundColor(Color.RED);

					alertboxToConfigure("ID CARD NO NOT IN DATABASE",  "If PC Number Correct? ENROLL");

					return;
				}


			}
			//tvInfo.setText(output);
			try {
				tinResults.clear();
				if(result!=null)
				{

					JSONArray jArray=new JSONArray(result);

					for(int i=0;i<jArray.length();i++)
					{
						JSONObject obj = jArray.getJSONObject(i);

						TinDetailsBean tbean=new TinDetailsBean();


						System.out.println("JSON OBJECT IS"+obj);

						tbean.setName(obj.getString("CriminalName"));
						tbean.setFathersName(obj.getString("phoneno"));
						tbean.setAddress(obj.getString("address"));
						tbean.setState(obj.getString("state"));
						tbean.setDistrict(obj.getString("district"));
						tbean.setType(obj.getString("type"));
						tbean.setTinNumber(obj.getString("tinNumber"));
						tbean.setPoliceStation(obj.getString("policestation"));
						tbean.setWebUrl(obj.getString("WebUploadedImageURL"));
						tbean.setTemplateURL(obj.getString("templateURL"));

						tbean.setDbid(obj.getString("dbid"));
						tbean.setUid(obj.getString("uid"));
						tbean.setScore(obj.getString("score"));




						tinResults.add(tbean);
						/*	criminalName=obj.getString("CriminalName");
							templateURL=obj.getString("templateURL");
							address=obj.getString("address");
							phoneNo=obj.getString("phoneno");
							latitude=obj.getString("latitude");
							longtitude=obj.getString("longtitude");
							personCategory=obj.getString("criminalCategory");
							webImage=obj.getString("WebUploadedImageURL");*/

					}


					//System.out.println("json recieved string:"+jsonString);
					//Blob blob = (Blob) obj.get("image");
					//int blobLength = (int) blob.length();
					//final byte[] imageBytes = blob.getBytes(1, blobLength);
					//final byte[] imageBytes =(byte[]) obj.get("image");
					//criminalName=obj.getString("CriminalName");
					if(jArray.length()>0){
						//tvInfo.setText("Matched Name is:"+criminalName);
						moreButton.setEnabled(true);
						moreButton.setText("Get More Info");
						moreButton.setBackgroundColor(Color.GREEN);
					}
				}


				/*tvInfo.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						try {
						try {// TODO Auto-generated method stub
						setContentView(R.layout.more_information);
						ByteArrayInputStream imageStream = new ByteArrayInputStream(imageBytes);
						Bitmap theImage= BitmapFactory.decodeStream(imageStream);
						URL url = new URL(address);
						InputStream content = (InputStream)url.getContent();
						Drawable d = Drawable.createFromStream(content , "src");
						iv.setImageDrawable(d)
						imageView.setImageBitmap(theImage);

						//nameOfTheCriminal.setText(obj.getString("CriminalName"));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});*/
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e2) {
				System.out.println("exception occured");
				e2.printStackTrace();
			}
			serverProcessDialogFragment.dismiss();

		}
		public String convertStreamToString(InputStream is) throws Exception {

			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			return sb.toString();
		}
		public String upload(String i_file)
		{
			String responseString = null;
			try
			{
				System.out.println("ENTERED INTO UPLOAD PICTURE TO SERVER for :"+deviceId);
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
				String imagePath="Template"+System.currentTimeMillis();
				if(imagePath!=null && deviceId!=null) {
					System.out.println("Hitting Server");

					//sending the imei number
					TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
					String IMEINumber = mngr.getDeviceId();

					//HttpPost httppost = new HttpPost("http://137.59.201.89:9092/childface/GetPictureFromClient?imageUrl="+imagePath+"&deviceId="+deviceId);

					double lattitude=0;
					double longitude=0;
					Location lastKnownLocation = null;


					LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

					List<String> providers = locationManager.getProviders(true);

					Location l = null;
					for (int i = 0; i < providers.size(); i++) {
						l = locationManager.getLastKnownLocation(providers.get(i));
						if (l != null)
							break;
					}
					if (l != null) {
						lattitude = l.getLatitude();
						longitude = l.getLongitude();
					}


					/*if(locationManager != null) {
						lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					}*/



					//HttpPost httppost = new HttpPost("http://137.59.201.89:9092/childface/GetPictureFromClient?imageUrl="+imagePath+"&deviceId="+deviceId+"&imei="+IMEINumber);

					//HttpPost httppost = new HttpPost("http://www.finsit.in:9092/childface/GetPictureFromClient?imageUrl="+imagePath+"&deviceId="+deviceId+"&imei="+IMEINumber+"&lat="+lattitude+"&lon="+longitude);
					HttpPost httppost = new HttpPost("http://134.119.179.70:9092/childface/GetFaceServlet?imageUrl="+imagePath+"&deviceId="+deviceId+"&imei="+IMEINumber+"&lat="+lattitude+"&lon="+longitude+"&pcNo="+pcnumber);

					//	HttpPost httppost = new HttpPost("http://198.27.68.95:9092/childface/GetPictureFromClient?imageUrl="+imagePath+"&deviceId="+deviceId+"&imei="+IMEINumber+"&lat="+lattitude+"&lon="+longitude);


					File file = new File(i_file);
					MultipartEntity mpEntity = new MultipartEntity();
					ContentBody cbFile = new FileBody(file, "image/jpeg");
					mpEntity.addPart("userfile", cbFile);
					httppost.setEntity(mpEntity);
					System.out.println("executing request " + httppost.getRequestLine());
					//tvInfo.setText("executing request !!");
					HttpResponse response = httpclient.execute(httppost);
					//tvInfo.setText("Response Got...processing now!!");
					HttpEntity resEntity = response.getEntity();
					InputStream stream=resEntity.getContent();
					responseString = convertStreamToString(stream);
					System.out.println("RESPONSE STRING IS:"+responseString);
					//tvInfo.setText("Response Finished!!");
					//Toast.makeText(getApplicationContext(),"Hello"+EntityUtils.toString(resEntity),Toast.LENGTH_LONG).show();
					System.out.println(response.getStatusLine());
	/*			    if (responseString != null) {
				      //System.out.println(EntityUtils.toString(resEntity));
				      //tvInfo.setText(EntityUtils.toString(resEntity));
				      //Toast.makeText(getApplicationContext(),EntityUtils.toString(resEntity),Toast.LENGTH_LONG).show();
				    }
				    if(responseString==null){
				    	responseString="NO MATCHES FOUND";
				    }*/
					if (resEntity != null) {
						resEntity.consumeContent();
					}
					httpclient.getConnectionManager().shutdown();
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			return responseString;

		}
	}

	@Override
	public void onClickStopBtn(DialogFragment dialogFragment) {
		// TODO Auto-generated method stub

		serverProcessDialogFragment.dismiss();

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(moreButton.getText().equals("Get More Info")){
			ProgressDialog progress = ProgressDialog.show(FaceScanActivity.this, "Retrieving Information", "Please wait...",true);
			System.out.println("Entered into onclick method");
			//if(moreButton.getba !=null){
			Intent intent = new Intent(getApplicationContext(),CustomListView.class);
			  /*  intent.putExtra("criminalName",criminalName);
			    intent.putExtra("templateURL", templateURL);
			    intent.putExtra("address", address);
			    intent.putExtra("phoneNo", phoneNo);
			    intent.putExtra("latitude",latitude);
			    intent.putExtra("longtitude",longtitude);
			    intent.putExtra("personCategory",personCategory);
			    intent.putExtra("webImage",webImage);*/

			Bundle info = new Bundle();
			//ArrayList<TinDetailsBean> mas = new ArrayList<TinDetailsBean>();
			info.putSerializable("results", tinResults);
			intent.putExtras(info);


			startActivity(intent);
			finish();

			//}
		}else{
			AlertDialog ad = new AlertDialog.Builder(this)
					.setMessage("Do you want to Enroll?")
					.setIcon(R.drawable.ic_launcher)
					.setTitle("Enroll Fingerprint")
					.setPositiveButton("Yes", this)
					.setNegativeButton("Cancel", this)
					.setCancelable(false)
					.create();
			ad.show();
		}

	}

	public static Bitmap fingerPrintImageBMP=null;

	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		switch(which){
			case DialogInterface.BUTTON_POSITIVE: // yes
				System.out.println("Entered into no match state");
				//Intent noMatchIntent=new Intent(this,StartPage.class);
				Intent noMatchIntent=new Intent(this,EnrollFaceActivity.class);

				/*BitmapDrawable bitmapDrawable = ((BitmapDrawable) img_fp_src.getDrawable());
				Bitmap bitmap = bitmapDrawable.getBitmap();*/
				//MyBitmapFile fileBMP = new MyBitmapFile(mImageWidth, mImageHeight, mImageFP);
				//Bitmap bitmap = BitmapFactory.decodeByteArray(fileBMP.toBytes() , 0, fileBMP.toBytes().length);
				//bitmap.com
				//noMatchIntent.putExtra("BitmapImage",bitmap);
				fingerPrintImageBMP=imgBitMap;

				noMatchIntent.putExtra("deviceId",deviceId);
				startActivity(noMatchIntent);

				break;
			case DialogInterface.BUTTON_NEGATIVE: // no
				//t.setText("You have denied the TOS. You may not access the site");
				break;
			default:
				// nothing
				break;
		}
	}



	public static byte[] rotateImageIfRequired(Context context, Uri uri, byte[] fileBytes) {
		byte[] data = null;
		Bitmap bitmap = BitmapFactory.decodeByteArray(fileBytes, 0, fileBytes.length);
		ByteArrayOutputStream outputStream = null;

		try {

			bitmap = rotateImageIfRequired(bitmap, context, uri);
			outputStream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
			data = outputStream.toByteArray();
		} catch (IOException e) {
			//Timber.e(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (outputStream != null) {
					outputStream.close();
				}
			} catch (IOException e) {
				// Intentionally blank
			}
		}

		return data;
	}


	public static Bitmap rotateImageIfRequired(Bitmap img, Context context, Uri selectedImage) throws IOException {

		/*if (selectedImage.getScheme().equals("content")) {
			String[] projection = { MediaStore.Images.ImageColumns.ORIENTATION };
			Cursor c = context.getContentResolver().query(selectedImage, projection, null, null, null);
			if (c.moveToFirst()) {
				final int rotation = c.getInt(0);
				c.close();
				return rotateImage(img, rotation);
			}
			return img;
		} else*/ {
			ExifInterface ei = new ExifInterface(selectedImage.getPath());
			int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
			//Timber.d("orientation: %s", orientation);

			switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_90:
					return rotateImage(img, 90);
				case ExifInterface.ORIENTATION_ROTATE_180:
					return rotateImage(img, 180);
				case ExifInterface.ORIENTATION_ROTATE_270:
					return rotateImage(img, 270);
				default:
					return img;
			}
		}
	}

	private static Bitmap rotateImage(Bitmap img, int degree) {
		Matrix matrix = new Matrix();
		matrix.postRotate(degree);
		Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
		return rotatedImg;
	}



	public Bitmap resizeBitmapFitXY(int width, int height, Bitmap bitmap){
		Bitmap background = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		float originalWidth = bitmap.getWidth(), originalHeight = bitmap.getHeight();
		Canvas canvas = new Canvas(background);
		float scale, xTranslation = 0.0f, yTranslation = 0.0f;
		if (originalWidth > originalHeight) {
			scale = height/originalHeight;
			xTranslation = (width - originalWidth * scale)/2.0f;
		}
		else {
			scale = width / originalWidth;
			yTranslation = (height - originalHeight * scale)/2.0f;
		}
		Matrix transformation = new Matrix();
		transformation.postTranslate(xTranslation, yTranslation);
		transformation.preScale(scale, scale);
		Paint paint = new Paint();
		paint.setFilterBitmap(true);
		canvas.drawBitmap(bitmap, transformation, paint);
		return background;
	}
}


